import {gql, GraphQLClient} from 'graphql-request'
import chalk from 'chalk'
import log from 'loglevel'
import prefix from 'loglevel-plugin-prefix'

/**
 * @typedef { import("gitlab-graphql-types").Group } Group
 * @typedef { import("gitlab-graphql-types").Project } Project
 * @typedef { import("gitlab-graphql-types").Label } Label
 * @typedef { import("gitlab-graphql-types").Epic } Epic
 * @typedef { import("gitlab-graphql-types").Issue } Issue
 * @typedef { import("gitlab-graphql-types").MergeRequest } MergeRequest
 *
 * @typedef { import("gitlab-graphql-types").Maybe } Maybe
 * @typedef { import("gitlab-graphql-types").PageInfo } PageInfo
 * @typedef { import("gitlab-graphql-types").Scalars } Scalars
 */

const args = process.argv.slice(2)

const client = new GraphQLClient('https://gitlab.com/api/graphql', {
  headers: {
    Authorization: `Bearer ${process.env.GITLAB_TOKEN}`,
  }
})

const colors = {
  TRACE: chalk.magenta,
  DEBUG: chalk.cyan,
  INFO: chalk.blue,
  WARN: chalk.yellow,
  ERROR: chalk.red,
};

prefix.reg(log)
log.setDefaultLevel(log.levels.INFO)

prefix.apply(log, {
  format(level, name, timestamp) {
    return `${chalk.gray(`[${timestamp}]`)} ${colors[level.toUpperCase()](level)} ${chalk.green(`${name}:`)}`;
  },
});

/**
 * Labels with their global IDs
 *
 * @type {Object.<Scalars["String"], Scalars["ID"]>}
 */
const labels = {}

/**
 * Gets the global ID of a label
 *
 * @param group {Scalars["ID"]} Full path of the namespace
 * @param title {Scalars["String"]}
 * @return {Promise<Maybe<Scalars["ID"]>>}
 */
async function getLabelId(group, title) {
  const labelHash = Buffer.from(title).toString('base64')
  if (!(labelHash in labels)) {
    log.debug(`Looking up label ID for ~"${title}"`)

    const query = gql`
      query GetLabel($group: ID!, $title: String!) {
        group(fullPath: $group) {
          label(title: $title) {
            id
            title
          }
        }
      }
    `

    /**
     * @type {{group: Group}}
     */
    const data = await client.request(query, {group, title})
    log.debug(labelHash, JSON.stringify(data))

    if (null !== data.group.label) {
      labels[labelHash] = data.group.label.id
      log.debug(`Retrieved ${labels[labelHash]} for ~"${data.group.label.title}"`)
    } else {
      labels[labelHash] = null;
      log.warn(`No label ~"${title}" found`)
    }
  }

  return labels[labelHash];
}

/**
 * Get all epics from a group
 *
 * @param group {Scalars["ID"]} Full path of the namespace
 * @return {Promise<Array<Epic>>}
 */
async function getEpics(group) {
  /**
   * @type {Array<Epic>}
   */
  const epics = []
  let cursor = null, hasNextPage

  do {
    log.debug(`Querying epics for group ${group}`)

    const query = gql`
      query GroupEpicsCursor($group: ID!, $cursor: String) {
        group(fullPath: $group) {
          id
          fullPath
          epics(state: all, includeDescendantGroups: true, after: $cursor) {
            nodes {
              id
              iid
              group {
                id
                fullPath
              }
              labels {
                nodes {
                  id
                  title
                }
              }
            }
            pageInfo {
              endCursor
              hasNextPage
            }
          }
        }
      }
    `

    /**
     * @type {{group: Group}}
     */
    const data = await client.request(query, {group, cursor})
    for (let epic of data.group.epics.nodes) {
      epics.push(epic)

      log.info(`Retrieved epic &${epic.iid} for group ${epic.group.fullPath}`)
    }

    cursor = data.group.epics.pageInfo.endCursor
    hasNextPage = data.group.epics.pageInfo.hasNextPage
  } while (hasNextPage)

  return epics
}

/**
 * Get all issues from a group
 *
 * @param group {Scalars["ID"]} Full path of the namespace
 * @return {Promise<Array<Issue|{project: Project}>>}
 */
async function getIssues(group) {
  /**
   * @type {Array<Issue>}
   */
  const issues = []
  let cursor = null, hasNextPage

  do {
    log.debug(`Querying issues for group ${group}`)

    const query = gql`
      query GroupIssuesCursor($group: ID!, $cursor: String) {
        group(fullPath: $group) {
          id
          fullPath
          issues(state: all, includeSubgroups: true, after: $cursor) {
            nodes {
              id
              iid
              projectId
              reference(full: true)
              labels {
                nodes {
                  id
                  title
                }
              }
            }
            pageInfo {
              endCursor
              hasNextPage
            }
          }
        }
      }
    `

    /**
     * @type {{group: Group}}
     */
    const data = await client.request(query, {group, cursor})
    for (let issue of data.group.issues.nodes) {
      issue.project = {
        id: issue.projectId,
        fullPath: issue.reference.split('#')[0],
      }

      issues.push(issue)
      log.info(`Retrieved issue #${issue.iid} for project ${issue.project.fullPath}`)
    }

    cursor = data.group.issues.pageInfo.endCursor
    hasNextPage = data.group.issues.pageInfo.hasNextPage
  } while (hasNextPage)

  return issues
}

/**
 * Get all merge requests from a group
 *
 * @param group {Scalars["ID"]} Full path of the namespace
 * @return {Promise<Array<MergeRequest>>}
 */
async function getMergeRequests(group) {
  /**
   * @type {Array<MergeRequest>}
   */
  const mergeRequests = []
  let cursor = null, hasNextPage

  do {
    log.debug(`Querying merge requests for group ${group}`)

    const query = gql`
      query GroupMergeRequestCursor($group: ID!, $cursor: String) {
        group(fullPath: $group) {
          id
          fullPath
          mergeRequests(state: all, includeSubgroups: true, after: $cursor) {
            nodes {
              id
              iid
              project {
                id
                fullPath
              }
              labels {
                nodes {
                  id
                  title
                }
              }
            }
            pageInfo {
              endCursor
              hasNextPage
            }
          }
        }
      }
    `

    /**
     * @type {{group: Group}}
     */
    const data = await client.request(query, {group, cursor})
    for (let mergeRequest of data.group.mergeRequests.nodes) {
      mergeRequests.push(mergeRequest)

      log.info(`Retrieved merge request !${mergeRequest.iid} in project ${mergeRequest.project.fullPath}`)
    }

    cursor = data.group.mergeRequests.pageInfo.endCursor
    hasNextPage = data.group.mergeRequests.pageInfo.hasNextPage
  } while (hasNextPage)

  return mergeRequests
}

/**
 * Updates an epic with the given new labels
 *
 * @param groupId {Scalars["ID"]} Group the epic to mutate is in
 * @param iid {Scalars["ID"]} IID of the epic to mutate
 * @param labels {Array<Scalars["ID"]>} IDs of labels to be added to the epic
 * @param oldLabels {Array<Scalars["ID"]>} IDs of labels to be removed from the epic
 * @return {Promise<void>}
 */
async function updateEpicLabels(groupId, iid, labels, oldLabels) {
  log.debug(`Updating epic &${iid} in group ${groupId}`)

  /**
   * @type {import("gitlab-graphql-types").UpdateEpicInput}
   */
  const input = {
    groupPath: groupId,
    iid: iid,
    addLabelIds: labels.filter(label => !oldLabels.includes(label)),
    removeLabelIds: oldLabels.filter(label => !labels.includes(label)),
  }

  const mutation = gql`
    mutation UpdateEpicLabels($input: UpdateEpicInput!) {
      updateEpic(input: $input) {
        epic {
          id
          iid
          group {
            id
            fullPath
          }
          labels {
            nodes {
              id
              title
            }
          }
        }
        errors
      }
    }
  `

  try {
    /**
     * @type {{updateEpic: import("gitlab-graphql-types").UpdateEpicPayload}}
     */
    const data = await client.request(mutation, {input})
    log.debug(JSON.stringify(data))

    log.info(`Updated epic &${data.updateEpic.epic.iid} in group ${data.updateEpic.epic.group.fullPath}`)
  } catch (error) {
    log.warn(error)
  }
}

/**
 * Updates an issue with the given new labels
 *
 * @param projectId {Scalars["ID"]} Project the issue to mutate is in
 * @param iid {Scalars["String"]} IID of the issue to mutate
 * @param labels {Array<Scalars["ID"]>} IDs of labels to be set
 * @return {Promise<void>}
 */
async function updateIssueLabels(projectId, iid, labels) {
  log.debug(`Updating issue #${iid} in project ${projectId}`)

  /**
   * @type {import("gitlab-graphql-types").UpdateIssueInput}
   */
  const input = {
    projectPath: projectId,
    iid: iid,
    labelIds: labels,
  }

  const mutation = gql`
    mutation UpdateIssueLabels($input: UpdateIssueInput!) {
      updateIssue(input: $input) {
        issue {
          id
          iid
          projectId
          reference(full: true)
          labels {
            nodes {
              id
              title
            }
          }
        }
        errors
      }
    }
  `

  try {
    /**
     * @type {{updateIssue: import("gitlab-graphql-types").UpdateIssuePayload}}
     */
    const data = await client.request(mutation, {input})
    log.debug(JSON.stringify(data))

    const project = {
      id: data.updateIssue.issue.projectId,
      fullPath: data.updateIssue.issue.reference.split('#')[0],
    }

    log.info(`Updated issue #${data.updateIssue.issue.iid} in project ${project.fullPath}`)
  } catch (error) {
    log.warn(error)
  }
}

/**
 * Updates a merge request with the given new labels
 *
 * @param projectId {Scalars["ID"]} Project the merge request to mutate is in
 * @param iid {Scalars["String"]}  IID of the merge request to update
 * @param labels {Array<Scalars["LabelID"]>} Label IDs to set
 * @return {Promise<void>}
 */
async function updateMergeRequestLabels(projectId, iid, labels) {
  log.debug(`Updating MR !${iid} in project ${projectId}`)

  /**
   * @type {import("gitlab-graphql-types").MergeRequestSetLabelsInput}
   */
  const input = {
    projectPath: projectId,
    iid: iid,
    labelIds: labels,
  }

  const mutation = gql`
    mutation UpdateMergeRequestLabels($input: MergeRequestSetLabelsInput!) {
      mergeRequestSetLabels(input: $input) {
        mergeRequest {
          id
          iid
          project {
            id
            fullPath
          }
          labels {
            nodes {
              id
              title
            }
          }
        }
        errors
      }
    }
  `

  try {
    /**
     * @type {{mergeRequestSetLabels: import("gitlab-graphql-types").MergeRequestSetLabelsPayload}}
     */
    const data = await client.request(mutation, {input})
    log.debug(JSON.stringify(data))

    log.info(`Updated merge request !${data.mergeRequestSetLabels.mergeRequest.iid} in project ${data.mergeRequestSetLabels.mergeRequest.project.fullPath}`)
  } catch (error) {
    log.warn(error)
  }
}

/**
 * Fetch new label IDs for the given labels
 *
 * If a label ID is not found, the label is removed from the result.
 *
 * @param group {Scalars["ID"]} Full path of the namespace
 * @param labels {Array<Label>} The current labels
 * @return {Promise<Array<Scalars["ID"]>>}
 */
async function fetchNewLabels(group, labels) {
  const labelIdPromises = []
  for (let label of labels) {
    if (label.id.includes('GroupLabel')) {
      labelIdPromises.push(getLabelId(group, label.title))
    } else {
      log.info(`Keeping ${label.id} for ~${label.title} for new labels`)
      labelIdPromises.push(Promise.resolve(label.id))
    }
  }

  const labelIds = await Promise.all(labelIdPromises);
  return labelIds.filter(Boolean)
}

/**
 * Update all epics in a group with new label IDs
 *
 * Labels are not found in the new group are removed.
 *
 * @param group {Scalars["ID"]} Full path of the namespace
 * @return {Promise<void>}
 */
async function updateEpics(group) {
  const epics = await getEpics(group)
  const updatePromises = []

  for (let epic of epics) {
    const labelIds = await fetchNewLabels(group, epic.labels.nodes)
    updatePromises.push(updateEpicLabels(epic.group.fullPath, epic.iid, labelIds, epic.labels.nodes.map(node => node.id)))
  }

  await Promise.all(updatePromises)
}

/**
 * Update all issues in a group with new label IDs
 *
 * Labels are not found in the new group are removed.
 *
 * @param group {Scalars["ID"]} Full path of the namespace
 * @return {Promise<void>}
 */
async function updateIssues(group) {
  const issues = await getIssues(group)
  const updatePromises = []

  for (let issue of issues) {
    const labelIds = await fetchNewLabels(group, issue.labels.nodes)
    updatePromises.push(updateIssueLabels(issue.project.fullPath, issue.iid, labelIds))
  }

  await Promise.all(updatePromises)
}

/**
 * Update all merge requests in a group with new label IDs
 *
 * Labels are not found in the new group are removed.
 *
 * @param group {Scalars["ID"]} Full path of the namespace
 * @return {Promise<void>}
 */
async function updateMergeRequests(group) {
  const mergeRequests = await getMergeRequests(group)
  const updatePromises = []

  for (let mergeRequest of mergeRequests) {
    const labelIds = await fetchNewLabels(group, mergeRequest.labels.nodes)
    updatePromises.push(updateMergeRequestLabels(mergeRequest.project.fullPath, mergeRequest.iid, labelIds))
  }

  await Promise.all(updatePromises)
}

await Promise.all([
  updateEpics(args[0]),
  updateIssues(args[0]),
  updateMergeRequests(args[0]),
])
