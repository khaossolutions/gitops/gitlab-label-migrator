# Label Migrator for GitLab

The Label Migrator for GitLab is a simple Node.js application to migrate GitLab group labels on epics, issues and merge
requests when projects where migrated into a new group.

The labels must already exist within the new group, for example created with the Terraform [`gitlab_group_label`](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group_label)
resource as the migrator works by replacing the same group labels from the previous group with group labels with the 
same label title from the new group.

WARNING: If a previous group label is not found in the new group, it is removed from the migrated object!

```bash
node gitlab-label-migrator.js <new-group-name>
```
